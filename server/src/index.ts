
import InsiderKäufe from "./insiderKäufer/index"
import initRSSController  from './ad-hoc/index'
import cron from 'node-cron'
import { SECDBAccess } from "./db/secDbAccess"
import logger from "./utils/Logger"


const init = async() => {
//await new SECDBAccess().create()
  
const adHoc = await  initRSSController()
const bafin = await InsiderKäufe.BafinRequest
const sec =  await InsiderKäufe.SECRequest


await adHoc.getData()
await bafin.run()
await sec.run()

cron.schedule("0 */2 * * *", async() => {
  await adHoc.parseData().catch(e => logger.error(e))
  await bafin.run().catch(e => logger.error(e))
  await sec.run().catch(e => logger.error(e))
})

}

init()





