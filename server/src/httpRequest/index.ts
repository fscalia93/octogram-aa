import axios from 'axios'

import axiosRetry from 'axios-retry';
import logger from '../utils/Logger';

axiosRetry(axios, { retryDelay: axiosRetry.exponentialDelay});

export const get = async(url: string) => {
  try{

    const resp = await axios.get(url)

    logger.info("Status:"+ resp.status)
  
    if(resp.status === 403){
        logger.info("Status 403: " + url)
    }
  
    return resp.data
  }

  catch(e){
    console.log(e)
    return ""
  }

 
}