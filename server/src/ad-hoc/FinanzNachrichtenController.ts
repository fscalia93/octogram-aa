
import { RSSController } from "../Controller/RSSController"
import { parseFinanzNachrichten } from "./parser"

const URL = 'https://www.finanznachrichten.de/rss-aktien-adhoc'
var source = "Finanznachrichten"


export const initRSSController   =async () => {
  const controller =RSSController.createAsync(URL, source, parseFinanzNachrichten)
 
  return controller
} 