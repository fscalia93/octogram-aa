import { AdHoc } from "../types/AdHoc";

export const parseFinanzNachrichten = (json: any, source: string) => {

  const adHocs: AdHoc[] = []


 for (let index = 0; index < json.length; index++) {



    var jsonObject: AdHoc = {
      "title": json[index].title[0],
      "pubDate":  json[index].pubDate[0],
      "description": json[index].description[0],
      "link": json[index].link[0],
      "source":source
  
         
    }
    adHocs.push(jsonObject)
 }
 return adHocs
  }