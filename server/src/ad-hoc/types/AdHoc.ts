export type AdHoc = {
  title: string,
  pubDate: string,
  description: string,
  link: string,
  source: string
}
