import { DerivativeTransaction, Issuer, ReportingOwner, TransactionTransactionAmounts } from "./SECType";

export type BaseEmittent = {
  issuer: Issuer,
  reportingOwner:  ReportingOwner,
  publicationDate: string,
  ISIN: string,
  id: string,
  status: string,
  instrumentType: string,
  businessType: string,
  transaction: DerivativeTransaction,
  transactionDate: string,
  tradingPlace: string,
  activationDate: string
}