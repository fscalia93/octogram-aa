export interface SECForm4 {
  periodOfReport:        string;
  id: string,
  tradingSymbol: string,
  notSubjectToSection16: string | undefined;
  issuer:                Issuer;
  reportingOwner:        ReportingOwner;
  nonDerivativeTable:    NonDerivativeTable | null;
  derivativeTable:       DerivativeTable | null;
  footnotes:             Footnotes | null;
  remarks:               string  | null;
  signatureName: string;
  signatureDate: string;
}

export interface DerivativeTable {
  derivativeTransaction: DerivativeTransaction[];
}

export interface DerivativeTransaction {
  securityTitle:             string;
  conversionOrExercisePrice: string;
  transactionDate:           string;
  deemedExecutionDate:       string;
  transactionFormType: string;
  transactionCode:     string;
  equitySwapInvolved:  string;
  transactionShares:               string;
  transactionPricePerShare:        string;
  transactionAcquiredDisposedCode: string;
  exerciseDate:              string;
  expirationDate:            string;
  underlyingSecurityTitle:  string;
  underlyingSecurityShares: string;
  sharesOwnedFollowingTransaction: string;
  directOrIndirectOwnership: string;
}

export interface ConversionOrExercisePrice {
  footnoteId: FootnoteID;
}

export interface FootnoteID {
  _id: string;
}

export interface ExpirationDate {
  value?:      string;
  footnoteId?: FootnoteID;
}

export interface OwnershipNature {
  directOrIndirectOwnership: string;
}

export interface SecurityTitle {
  value: string;
}

export interface PostTransactionAmounts {
  sharesOwnedFollowingTransaction: string;
}

export interface TransactionTransactionAmounts {
  transactionShares:               string;
  transactionPricePerShare:        string;
  transactionAcquiredDisposedCode: string;
}

export interface TransactionCoding {
  transactionFormType: string;
  transactionCode:     string;
  equitySwapInvolved:  string;
}

export interface UnderlyingSecurity {
  underlyingSecurityTitle:  string;
  underlyingSecurityShares: string;
}

export interface Footnotes {
  footnote: Footnote[];
}

export interface Footnote {
  id:    string;
  text: string;
}

export interface Issuer {
  cik:           string;
  name:          string;
  tradingSymbol: string;
}

export interface NonDerivativeTable {
  nonDerivativeTransaction: NonDerivativeTransaction[];
}

export interface NonDerivativeTransaction {
  securityTitle:          string;
  transactionDate:        string;
  deemedExecutionDate:    string;
  transactionShares:               string;
  transactionPricePerShare:        string;
  transactionAcquiredDisposedCode: string;
  transactionFormType: string;
  transactionCode:     string;
  equitySwapInvolved:  string;
  sharesOwnedFollowingTransaction: string;
  directOrIndirectOwnership: string;
}

export interface OwnerSignature {
  signatureName: string;
  signatureDate: string;
}

export interface ReportingOwner {
  cik:  string;
  name: string;
  reportingOwnerAddress:      ReportingOwnerAddress;
  reportingOwnerRelationship: ReportingOwnerRelationship;
}

export interface ReportingOwnerAddress {
  rptOwnerStreet1:          string;
  rptOwnerStreet2:          string;
  rptOwnerCity:             string;
  rptOwnerState:            string;
  rptOwnerZipCode:          string;
  rptOwnerStateDescription: string;
}

export interface ReportingOwnerID {
  rptOwnerCik:  string;
  rptOwnerName: string;
}

export interface ReportingOwnerRelationship {
  isDirector:        string;
  isOfficer:         string;
  isTenPercentOwner: string;
  isOther:           string;
  officerTitle:      string;
}
