import { BaseIssuer, BaseReporter } from "./Base"
import { BaseEmittent } from "./BaseEmittent"
import { DerivativeTransaction, Issuer, ReportingOwner } from "./SECType"

export type BafinIssuer = BaseIssuer & {
  ISIN: string,
}

export type BafinReporter  = BaseReporter & {
     
}


export type BafinEmittent =  {
  issuerName: string,
  reportingOwner:  BafinReporter,
  publicationDate: string,
  issuerISIN: string,
  id: string,
  reportingOwnerStatus: string,
  transactionAcquiredDisposedCode: string,
  businessType: string,
  transactionPricePerShare: string,
  transactionShares: string,
  transactionDate: string,
  tradingPlace: string,
  activationDate: string,
  source: string
}