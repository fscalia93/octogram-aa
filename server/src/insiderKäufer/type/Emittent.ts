import { BaseEmittent } from "./BaseEmittent"
import { DerivativeTransaction, Issuer, ReportingOwner } from "./SECType"

export type Emittent =  {
  issuer: Issuer,
  reportingOwner:  ReportingOwner,
  publicationDate: string,
  ISIN: string,
  id: string,
  transactionAcquiredDisposedCode: string,
  instrumentType: string,
  businessType: string,
  transactionPricePerShare: string,
  transactionShares: string,
  transactionDate: string,
  tradingPlace: string,
  activationDate: string,
  source: string
}