export type BaseIssuer = {
  name: string,
  type: string
}

export type BaseReporter = {
  name: string,
  type: string
}