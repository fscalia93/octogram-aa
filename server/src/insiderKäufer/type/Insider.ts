type Insider = {
  emitent: string,
  id: string,
  ISIN: string,
  meldePflichtiger: string,
  status: string,
  typeInstrument: string,
  typeGeschäft: string,
  dateGeschäft: string,
  locationGeschäft: string,
  dateActivation: string
}