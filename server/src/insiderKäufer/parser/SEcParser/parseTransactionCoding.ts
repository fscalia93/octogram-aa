import { TransactionCoding } from "../../type/SECType"

export const parseTransactionCoding = (json: any) => {

        
  const transactionCoding: TransactionCoding = {
      equitySwapInvolved: json.equitySwapInvolved[0],
      transactionCode: json.transactionCode[0],
      transactionFormType: json.transactionFormType[0]

  }
  return transactionCoding
}