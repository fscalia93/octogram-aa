import { get } from "../../../httpRequest";
import xml2js from "xml2js";
import { BaseParser } from "../BaseParser";
import {
  DerivativeTable,
  DerivativeTransaction,
  Issuer,
  NonDerivativeTable,
  NonDerivativeTransaction,
  ReportingOwner,
  ReportingOwnerAddress,
  ReportingOwnerID,
  ReportingOwnerRelationship,
  SECForm4,
  UnderlyingSecurity,
} from "../../type/SECType";
import { parseOwnerShipNature } from "./parseOwnerShip";
import { parseTransactionCoding } from "./parseTransactionCoding";
import { parsePostTransactionAmounts } from "./parsePostTransactionAmounts";
import { parseTransactionAmounts } from "./parseTransactionAmounts";
import { parseOwnerSignature } from "./parseOwnerSignature";
import { parseFootNotes } from "./parseFootNotes";
import { exit } from "process";
import { executionAsyncId } from "async_hooks";
import logger from "../../../utils/Logger";

const rootURL = "https://www.sec.gov";

export class SECParser extends BaseParser<any, SECForm4> {
  private parseIssuer = (json: any) => {
    const issuer: Issuer = {
      cik: json.issuerCik[0],
      name: json.issuerName[0],
      tradingSymbol: json.issuerTradingSymbol[0],
    };
    return issuer;
  };

  private parseReportingOwner = (json: any) => {
    const parseReportingOwnerId = () => {
      const reportingOwnerId: ReportingOwnerID = {
        rptOwnerCik: json.reportingOwnerId[0].rptOwnerCik[0],
        rptOwnerName: json.reportingOwnerId[0].rptOwnerName[0],
      };
      return reportingOwnerId;
    };
    const parseReportingOwnerAddress = () => {
      const reportingOwnerAdress: ReportingOwnerAddress = {
        rptOwnerStreet1: json.reportingOwnerAddress[0].rptOwnerStreet1[0],
        rptOwnerStreet2: json.reportingOwnerAddress[0].rptOwnerStreet2[0],
        rptOwnerCity: json.reportingOwnerAddress[0].rptOwnerCity[0],
        rptOwnerState: json.reportingOwnerAddress[0].rptOwnerState[0],
        rptOwnerZipCode: json.reportingOwnerAddress[0].rptOwnerZipCode[0],
        rptOwnerStateDescription:
          json.reportingOwnerAddress[0].rptOwnerStateDescription[0],
      };
      return reportingOwnerAdress;
    };

    const parseReportingOwnerRelationShip = () => {
      const reportingOwnerRelationship: ReportingOwnerRelationship = {
        isDirector:  json.reportingOwnerRelationship[0].isDirector ?  json.reportingOwnerRelationship[0].isDirector[0]: 0,
        isOfficer: json.reportingOwnerRelationship[0].isOfficer ? json.reportingOwnerRelationship[0].isOfficer[0] : 0,
        isOther:  json.reportingOwnerRelationship[0].isOther? json.reportingOwnerRelationship[0].isOther[0] : 0,
        isTenPercentOwner: json.reportingOwnerRelationship[0].isTenPercentOwner ? 
          json.reportingOwnerRelationship[0].isTenPercentOwner[0] : 0,
        officerTitle:  json.reportingOwnerRelationship[0].officerTitle ? json.reportingOwnerRelationship[0].officerTitle[0] : 0,
      };
      return reportingOwnerRelationship;
    };

    const reportingOwnerId =parseReportingOwnerId()
    const reportingOwner: ReportingOwner = {
      name: reportingOwnerId.rptOwnerName,
      cik: reportingOwnerId.rptOwnerCik,
      reportingOwnerAddress: parseReportingOwnerAddress(),
      reportingOwnerRelationship: parseReportingOwnerRelationShip(),
    };
    return reportingOwner;
  };

  private parseNonDerivateTable = (json: any) => {
    const parseNonDerivateTransactions = (
      json: any
    ): NonDerivativeTransaction[] => {
      const nonDerivateTransactions: NonDerivativeTransaction[] = [];

    
      for (let index = 0; index < json.length; index++) {
        const actualTransaction = json[index];


        const ownerShipNature = parseOwnerShipNature(
          actualTransaction.ownershipNature[0]
        )

        const postTransactionAmounts = parsePostTransactionAmounts(
          actualTransaction.postTransactionAmounts[0]
        )

        const transactionAmounts = parseTransactionAmounts(
          actualTransaction.transactionAmounts[0]
        )

        const transactionCoding = parseTransactionCoding(
          actualTransaction.transactionCoding[0]
        )

        const nonDerivateTransaction: NonDerivativeTransaction = {
          securityTitle: actualTransaction.securityTitle[0].value[0],
          deemedExecutionDate: actualTransaction.deemedExecutionDate ? actualTransaction.deemedExecutionDate : null,
          ...ownerShipNature,
          ...postTransactionAmounts,
          ...transactionAmounts,
          ...transactionCoding,
          transactionDate: actualTransaction.transactionDate[0].value[0],
        };

        nonDerivateTransactions.push(nonDerivateTransaction);
      }
      return nonDerivateTransactions;
    };

    if (json === "" || !json.nonDerivativeTransaction) {
      return null;
    } else {
      const nonderivateTable: NonDerivativeTable = {
        nonDerivativeTransaction: parseNonDerivateTransactions(
          json.nonDerivativeTransaction
        ),
      };

      return nonderivateTable;
    }
  };

  private parseDerivateTable = (json: any) => {
    const parseDerivateTransactions = (json: any) => {
      const parseUnderliyingSecurities = (json: any) => {
        const underlyingSecurity: UnderlyingSecurity = {
          underlyingSecurityShares: json.underlyingSecurityShares[0],
          underlyingSecurityTitle: json.underlyingSecurityTitle[0],
        };
        return underlyingSecurity;
      };

      const derivateTransactions: DerivativeTransaction[] = [];
      for (let index = 0; index < json.length; index++) {
        const actualTransaction = json[index];

        const ownerShipNature = parseOwnerShipNature(
          actualTransaction.ownershipNature[0]
        )

        const postTransactionAmounts = parsePostTransactionAmounts(
          actualTransaction.postTransactionAmounts[0]
        )

        const transactionAmounts = parseTransactionAmounts(
          actualTransaction.transactionAmounts[0]
        )

        const transactionCoding =  parseTransactionCoding(
          actualTransaction.transactionCoding[0]
        )

        const underlyingSecurities = parseUnderliyingSecurities(
          actualTransaction.underlyingSecurity[0]
        )

        const derivateTransaction: DerivativeTransaction = {
          securityTitle: actualTransaction.securityTitle[0].value[0],
          deemedExecutionDate: actualTransaction.deemedExecutionDate ? actualTransaction.deemedExecutionDate : null,
          transactionDate: actualTransaction.transactionDate[0].value[0],
          conversionOrExercisePrice:
            actualTransaction.conversionOrExercisePrice[0],
          exerciseDate: actualTransaction.exerciseDate[0],
          expirationDate: actualTransaction.expirationDate[0],
          directOrIndirectOwnership: ownerShipNature.directOrIndirectOwnership,
          ...postTransactionAmounts,
          ...transactionAmounts,
          ...transactionCoding,
          underlyingSecurityShares: underlyingSecurities.underlyingSecurityShares,
          underlyingSecurityTitle: underlyingSecurities.underlyingSecurityTitle
            
        
        };

        derivateTransactions.push(derivateTransaction);
      }
      return derivateTransactions;
    };


    if (json === "") {
      return null;
    } else {

  
      const derivateTransaction =  json.derivativeTransaction ?  parseDerivateTransactions(json.derivativeTransaction[0]) : []
      "TODOO TRANSAKTIONHolding IS MISSING"
      

      const derivateTable: DerivativeTable = {
        derivativeTransaction: derivateTransaction,
      };

      return derivateTable;
    }
  };

  parse = async (json: any): Promise<SECForm4[]> => {
    const data: SECForm4[] = [];

    const getForm4URL = (stringRes: string) => {
      const regEx = new RegExp("href=.*.xml");

      const regExRes = stringRes.match(regEx);

      if (regExRes) {
        const hrefText = regExRes[0];
        let cleanedString = hrefText.replace('href="', "");
      
        let spliitedString = cleanedString.split("/");

        if (spliitedString[spliitedString.length - 2].match("^([^0-9])*")) {
          cleanedString = cleanedString.replace(
            spliitedString[spliitedString.length - 2] + "/",
            ""
          );
        }
    
        return rootURL + cleanedString;
      }
      return null;
    };

    const getForm4XML = async (fullUrl: string) => {
      const form4Res = await get(fullUrl);
      const obj = await xml2js.parseStringPromise(form4Res);

      return obj;
    };

    for (let index = 0; index < json.length; index++) {
      const href = json[index].link[0]["$"].href;
      const res = await get(href);
      const url = getForm4URL(res as string);
      if (!url) continue;
      logger.info("URL:"+ url);

      const obj = await getForm4XML(url);

    
    

      try {
        const issuer = this.parseIssuer(obj.ownershipDocument.issuer[0]);
        const reportingOwner = this.parseReportingOwner(
          obj.ownershipDocument.reportingOwner[0]
        );
        const nonDerivativeTable = obj.ownershipDocument.nonDerivativeTable ? this.parseNonDerivateTable(
          obj.ownershipDocument.nonDerivativeTable[0]) : null
        

       
        const derivativeTable = obj.ownershipDocument.derivativeTable ? this.parseDerivateTable(
          obj.ownershipDocument.derivativeTable[0]) : null

        const notSubjectToSection16 = obj.ownershipDocument.notSubjectToSection16 ? obj.ownershipDocument.notSubjectToSection16[0] : undefined
         
          const ownerSignature = parseOwnerSignature(obj.ownershipDocument.ownerSignature[0])


          const footNotes = obj.ownershipDocument.footnotes ?  parseFootNotes(obj.ownershipDocument.footnotes[0]) : null

        const jsonObj: SECForm4 = {
          issuer,
          id: issuer.cik,
          tradingSymbol: issuer.tradingSymbol,
          reportingOwner,
          derivativeTable,
          nonDerivativeTable,
          footnotes: footNotes,
          notSubjectToSection16,
          signatureDate: ownerSignature.signatureDate,
          signatureName: ownerSignature.signatureName,         
          periodOfReport: obj.ownershipDocument.periodOfReport[0],
          remarks: obj.ownershipDocument.remarks ? obj.ownershipDocument.remarks[0]: null,
        };

        data.push(jsonObj);
      } catch (e: any) {
        logger.error("Error:", e);
      } finally { }
      
    }

    return data;
  };
}
