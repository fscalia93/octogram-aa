import { PostTransactionAmounts } from "../../type/SECType"

export const parsePostTransactionAmounts = (json: any) => {
  const ownerShipNatur: PostTransactionAmounts = {
    sharesOwnedFollowingTransaction: json.sharesOwnedFollowingTransaction[0].value[0]
  }
  return ownerShipNatur
}