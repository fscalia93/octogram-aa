import {TransactionTransactionAmounts } from "../../type/SECType"

export  const parseTransactionAmounts = (json: any) => {

   const nonDerivativeTransactionTransactionAmounts: TransactionTransactionAmounts = {
    transactionAcquiredDisposedCode: json.transactionAcquiredDisposedCode[0].value ? json.transactionAcquiredDisposedCode[0].value[0]: null,
    transactionPricePerShare: json.transactionPricePerShare[0].value ? json.transactionPricePerShare[0].value[0] : null,
    transactionShares: json.transactionShares[0].value ? json.transactionShares[0].value[0] : null

  }

  return nonDerivativeTransactionTransactionAmounts

}