import { OwnershipNature, OwnerSignature } from "../../type/SECType"

export const parseOwnerSignature = (json: any) => {
  const ownerSignaturer: OwnerSignature = {
    signatureDate: json.signatureDate[0],
    signatureName: json.signatureName[0]
   
  }
  return ownerSignaturer
}