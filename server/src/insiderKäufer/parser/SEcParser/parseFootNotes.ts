import { Footnote, Footnotes } from "../../type/SECType"

export const parseFootNotes = (json: any) => {

  

  const parseFootNote = (json: any) => {
 

    const footNotes: Footnote[] = []
    for(let index = 0; index < json.length; index ++){

      const actualFootNote = json[index]

      
      const footNote: Footnote = {
        text: actualFootNote["_"],
        id: actualFootNote["$"].id
      }

      footNotes.push(footNote)
    }
    return footNotes
  }

  if(json  === '') return null 
  const footNotes: Footnotes = {
    footnote: parseFootNote(json.footnote)
  }
  
  return footNotes
}