import { OwnershipNature } from "../../type/SECType"

export const parseOwnerShipNature = (json: any) => {
  const ownerShipNatur: OwnershipNature = {
    directOrIndirectOwnership: json.directOrIndirectOwnership[0].value[0]
  }
  return ownerShipNatur
}