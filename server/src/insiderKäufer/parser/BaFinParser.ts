import { BafinEmittent, BafinIssuer, BafinReporter } from "../type/BafinEmittent"
import { Emittent } from "../type/Emittent"
import { Issuer } from "../type/SECType"
import { BaseParser } from "./BaseParser"

export class BafinParser extends BaseParser<any, BafinEmittent> {

  public parse = async(json: any) => {

  const types =  json[0]
  const data = json
  const insiders: BafinEmittent[] = []

  for (let index = 1; index < json.length; index++) {


    const jsonObject: BafinEmittent = {
        issuerName: data[index].column[0],
        source: this.source,
        id: data[index].column[1],
        issuerISIN: data[index].column[2],
        reportingOwner: data[index].column[3],
        reportingOwnerStatus: data[index].column[4],
        transactionAcquiredDisposedCode:  data[index].column[5],
        businessType: data[index].column[6],
        transactionPricePerShare: data[index].column[7],
        transactionShares: data[index].column[8],
        publicationDate: data[index].column[9],
        transactionDate: data[index].column[10],
        tradingPlace: data[index].column[11],
        activationDate: data[index].column[12]
      }

    insiders.push(jsonObject)
    }

    return insiders

  }
}