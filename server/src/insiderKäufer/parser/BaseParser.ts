export abstract class BaseParser<input, output>{

  source: string

  constructor(source: string){
    this.source = source
  }

  abstract parse(data: input): Promise<output[]>




}