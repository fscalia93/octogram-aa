import { get } from "../../httpRequest"
import xml2js from 'xml2js'
import {BaseRequest} from './BaseRequest'
import { SECParser} from "../parser/SEcParser/SECParser"
import { DbAccess } from "../../db/baseDbAccess"
import { SECForm4 } from "../type/SECType"
import { SECDBAccess } from "../../db/secDbAccess"
import logger from "../../utils/Logger"

const URL =  "https://www.sec.gov/cgi-bin/browse-edgar?action=getcurrent&CIK=&type=4&company=&dateb=&owner=only&start=0&count=100000&output=atom"
const SOURCE = "SEC"

export class SECRequest extends  BaseRequest<SECForm4> {
  



  public static create = async(url: string, source: string, parser: SECParser, dbAcess: SECDBAccess) => {

    const me = new SECRequest()
    me.dbAcess = dbAcess
    me.url = url
    me.source = source
    me.parser = parser


    return me;
  }



  public  run = async() => {

    
    const res = await get(this.url)
       const obj = await xml2js.parseStringPromise(res)

       if(obj){
        
        const data = await this.parser!.parse(obj.feed.entry )
          if(data.length > 0){
          await this.pushtoDb(data)
        }
       }
       else{
         logger.error("Error parsing SEC Data")
       }
  
   
  }



}

export default  SECRequest.create(URL, SOURCE, new SECParser(SOURCE), new SECDBAccess())