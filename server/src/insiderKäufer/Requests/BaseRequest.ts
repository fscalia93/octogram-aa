import { DbAccess } from "../../db/baseDbAccess"
import { BaseParser } from "../parser/BaseParser"

export abstract class BaseRequest<t>{
  url: string = ""
  source: string = ""
  parser: BaseParser<any, any> |undefined
  dbAcess: DbAccess<t> | undefined


  constructor(){

  }

   public abstract run(): Promise<void>

   getFromDb = async() => {
    return await this.dbAcess!.getElements()
   }

   protected pushtoDb = async(elements: t[]) => {
    await this.dbAcess?.addList(elements)
    }
    
}