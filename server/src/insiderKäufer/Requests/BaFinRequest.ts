import { get } from "../../httpRequest"
import xml2js from 'xml2js'
import { BafinParser } from "../parser/BaFinParser"
import {BaseRequest} from './BaseRequest'
import { Emittent } from "../type/Emittent"
import {  InsiderDBAccess } from "../../db/bafinDBAccess"
import { DbAccess } from "../../db/baseDbAccess"
import { BafinEmittent } from "../type/BafinEmittent"
import logger from "../../utils/Logger"

const URL =  "https://portal.mvp.bafin.de/database/DealingsInfo/sucheForm.do?meldepflichtigerName=&zeitraum=0&d-4000784-e=3&emittentButton=Suche+Emittent&emittentName=&zeitraumVon=&emittentIsin=&6578706f7274=1&zeitraumBis="
const SOURCE = "BaFin"


export class BafinRequest extends  BaseRequest<BafinEmittent> {


  public static create = async(url: string, source: string, parser: BafinParser, dbAcess: DbAccess<BafinEmittent>) => {

    const me = new BafinRequest()
    me.dbAcess = dbAcess
    me.url = url
    me.source = source
    me.parser = parser
    return me;
  }


  public async run(){
    const res = await get(this.url) 
    const obj = await xml2js.parseStringPromise(res)

    if(this.parser){
      const data =  await this.parser.parse(obj.table.row)
       
      if(data.length > 0){
        await this.pushtoDb(data)
      }
    }
   
  }

}

export default  BafinRequest.create(URL, SOURCE, new BafinParser(SOURCE), new InsiderDBAccess())