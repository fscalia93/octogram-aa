import xml2js from 'xml2js'
import { get } from '../httpRequest/index'
import { AdHoc } from '../ad-hoc/types/AdHoc'
import { AdHocAccess } from '../db/adHocDbAcess'
import logger from '../utils/Logger'



const URL = 'https://www.finanznachrichten.de/rss-aktien-adhoc'
var source = "Finanznachrichten"

export class RSSController {

  url: string = ''
  source: string = ''
  data: AdHoc[] = []
  parser: ((res: any, source: string) => AdHoc[]) | null = null
  dbAcess = new AdHocAccess()

  constructor (){
    
  }

  public static createAsync = async(url: string, source: string, parser: (res: any, source: string) => AdHoc[]) => {

    const me = new RSSController()
    me.data =  await me.getFromDb()
    me.url = url
    me.source = source
    me.parser = parser

    return me;

  }


  public async parseData() {

    const data = await get(this.url) 
    const obj = await xml2js.parseStringPromise(data)

    const adHocMessages = obj.rss.channel[0].item


    if(this.parser){
      const adhocs = this.parser(adHocMessages, source)
      this.addData(adhocs)
    }


  }

  private pushtoDb = async(adHocs: AdHoc[]) => {
    
      logger.info("push to db"+ "RSS"+ adHocs.length)
      await this.dbAcess.addList(adHocs)
  }

  private addData(adHocs: AdHoc[]){

    logger.info("addData " + adHocs.length + "this data " + this.data.length)



   const filteredAdHocs = adHocs.filter(x => {
     for(let index = 0; index < this.data.length; index++){
        if(this.data[index].title === x.title){
          return false
        }
     }
     return true
   })

   logger.info("filtered" +filteredAdHocs.length)

    this.data = this.data.concat(filteredAdHocs)

    if(filteredAdHocs.length > 0){
      this.pushtoDb(filteredAdHocs)
    }

   
  }

  public getData(){
    return this.data
   }

   private getFromDb = async() => {
     return await this.dbAcess.getElements()
   }

}
