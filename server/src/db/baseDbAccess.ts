
import { Knex } from "knex"
import logger from "../utils/Logger"
import knexInstance from "./knexInitialiser"


export abstract class DbAccess<t> {

  dbName: string = ""
  knexInstance: Knex<any, unknown> = knexInstance

  constructor ( ){
    
  }

  public add(element : t){
    this.insertOrUpdateWithoutUnique(this.dbName, [element])
  }

  public addList = async(elements: t[]) => {

    
    await this.knexInstance(this.dbName).insert(elements)
    
  }


  protected  insertOrUpdate = async<k>(tableName: string, elementToUpdate: k, id: string ) => {
    await this.knexInstance(tableName).insert(elementToUpdate).onConflict(id)
    .merge()
    .catch( err => {
      logger.error(`There was an error upserting the "${this.dbName}" table by ${this.dbName}:`, err)
      throw err
    })
}

protected insertOrUpdateWithoutUnique = async<k>(tableName: string, elementToUpdate :k[]) =>{

  for(let index = 0; index < elementToUpdate.length; index++){
    const element = elementToUpdate[index]

    const res = await this.knexInstance(tableName).where(element) as k[]
  
    if(res.length != 1){
      await this.knexInstance(tableName).insert(element).catch(e => console.log(e))
    }
    else{
      res.length === 1 && console.log("Entry already exists")
      res.length !== 1 && console.log("Error inserting entry")
    
    }
  }


}
  
public getElements  =  async() => {
  return await this.knexInstance<t>(this.dbName) as t[]
}

  abstract updateElement(element: t): Promise<void>;


abstract create(): Promise<void>;

}

