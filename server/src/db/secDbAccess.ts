import { X_OK } from "constants"
import { exit } from "process"
import { Emittent } from "../insiderKäufer/type/Emittent"
import { SECForm4 } from "../insiderKäufer/type/SECType"
import logger from "../utils/Logger"
import { DbAccess } from "./baseDbAccess"
import knexInstance from "./knexInitialiser"
import { DBNonDeriviateTable } from "./types/DBNonDeriviateTable"
import { DBReportingOwner } from "./types/DBReportingOwner"
import { DBSECForm4 } from "./types/DBSECForm4"

const db = 'insiderSEC'

export class SECDBAccess extends DbAccess<SECForm4>{

  dbName = db
  dbNameIssuer = db+"-issuer"
  dbNameFootNote = db+"-footnote"
  dbNameReportingOwner = db+"-reportingOwner"
  dbNameDeriviateTable = db+"-derivativeTable"
  dbNameNonDeriviateTable = db+"-nonDerivativeTable"




  updateElement = async(element: SECForm4): Promise<void> => {
   // await this.knexInstance(db).where({ISIN: element.ISIN, pubDate: element.id}).update(element)
  }

  private convertReportingOWner = (element: SECForm4) => {

    const res: DBReportingOwner = {
      id: element.reportingOwner.cik,
      name: element.reportingOwner.name,
      ...element.reportingOwner.reportingOwnerAddress,
    
    }

    return res;
  }

  private convertNonDeriviateTransaction = (element: SECForm4) => {
    
    if(!element ||! element.nonDerivativeTable  ) return null

    const res: DBNonDeriviateTable[] = 
      element.nonDerivativeTable.nonDerivativeTransaction.map(x => { return {...x, id: element.id}})

    return res
   
  }

  private convertDeriviateTransaction = (element: SECForm4) => {

    if(!element || ! element.derivativeTable  ) return null


    const res: DBNonDeriviateTable[] = 
      element.derivativeTable.derivativeTransaction.map(x => { return {...x, id: element.id}})

    return res

  }


  public addList = async(elements: SECForm4[]) => {


    for(let index = 0; index < elements.length; index ++){
      const element = elements[index]
      const dbElement: DBSECForm4 = {
        periodOfReport: element.periodOfReport,
        id: element.id,
        tradingSymbol: element.tradingSymbol,
        notSubjectToSection16: element.notSubjectToSection16 ?  element.notSubjectToSection16: null,
        reportingOwnerId: element.reportingOwner.cik,
        remarks: element.remarks ? element.remarks: null,
        ownerSignatureName: element.signatureName,
        ownerSignatureDate: element.signatureDate,
        ...element.reportingOwner.reportingOwnerRelationship 
      }
      
    
    const dbIssuer = {
      name: element.issuer.name,
      tradingSymbol: element.issuer.tradingSymbol
    }
  
    const reportingOwner =  this.convertReportingOWner(element)
   
    await this.insertOrUpdateWithoutUnique(this.dbNameReportingOwner, [reportingOwner])

    await this.insertOrUpdate(this.dbNameIssuer, dbIssuer, "tradingSymbol")

    await this.insertOrUpdate(this.dbName, dbElement, "id")

    const derivitateTable = this.convertDeriviateTransaction(element)

   
    if(derivitateTable && derivitateTable.length  > 0){
      await this.insertOrUpdateWithoutUnique(this.dbNameDeriviateTable, derivitateTable)
    } 


    const nonDeriviateTable = this.convertNonDeriviateTransaction(element)
    
    console.log("Non Deriviate Table", JSON.stringify(nonDeriviateTable))
   

    if(nonDeriviateTable && nonDeriviateTable.length  > 0){
      await this.insertOrUpdateWithoutUnique(this.dbNameNonDeriviateTable, nonDeriviateTable)
    } 
     
  }
    
  
  }

  createReportingOwner = async() => {
    await  knexInstance.schema.createTable(this.dbNameReportingOwner, (table) => {
      table.string('id').primary()
      table.string('name')
      table.string('rptOwnerStreet1').nullable()
      table.string('rptOwnerStreet2').nullable()
      table.string('rptOwnerCity').nullable()
      table.string('rptOwnerState').nullable()
      table.string('rptOwnerZipCode').nullable()
      table.string('rptOwnerStateDescription').nullable()
     
   
    }).catch(e => logger.info(e))
  }

  createInsiderSEC = async() => {

    await knexInstance.schema.createTable(db, (table) => {
      table.string('periodOfReport')
      table.string('id').primary()
      table.string('tradingSymbol').references(`${this.dbNameIssuer}.tradingSymbol`)
      table.string('notSubjectToSection16')
      table.string('reportingOwnerId').references(`${this.dbNameReportingOwner}.reportingOwnerId`)
      table.string('remarks')
      table.string('ownerSignatureName')
      table.string('ownerSignatureDate')
      table.string('isDirector').nullable()
      table.string('isOfficer').nullable()
      table.string('isTenPercentOwner').nullable()
      table.string('isOther').nullable()
      table.string('officerTitle').nullable()
     
    }).catch(e => logger.info(e))

  }

  create = async(): Promise<void> => {
   
    await this.createReportingOwner()

    await knexInstance.schema.createTable(this.dbNameIssuer, (table) => {
      table.string('name')
      table.string('tradingSymbol').primary()
    }).catch(e => logger.info(e))

   /* await knexInstance.schema.createTable(db+"-footnote", (table) => {
        table.string('footNoteId').references(`${db}.id`)
        table.string('text')
      })
      */

      await this.createInsiderSEC()


   


          await  knexInstance.schema.createTable(this.dbNameDeriviateTable, (table) => {
            table.string('id').references(`${db}.id`)
            table.string('securityTitle').nullable()
            table.string('conversionOrExercisePrice').nullable()
            table.string('transactionDate').nullable()

            table.string('deemedExecutionDate').nullable()
            table.string('transactionFormType').nullable()
            table.string('transactionCode').nullable()

            table.string('equitySwapInvolved').nullable()
            table.string('transactionShares').nullable()
            table.string('transactionPricePerShare').nullable()

            table.string('transactionAcquiredDisposedCode').nullable()
            table.string('exerciseDate').nullable()
            table.string('expirationDate').nullable()

            table.string('underlyingSecurityTitle').nullable()
            table.string('underlyingSecurityShares').nullable()
            table.string('sharesOwnedFollowingTransaction').nullable()
            table.string('directOrIndirectOwnership').nullable()
          }).catch(e => logger.info(e))


          await  knexInstance.schema.createTable(this.dbNameNonDeriviateTable, (table) => {
              table.string('id').references(`${db}.id`)
              table.string('securityTitle').nullable()
              table.string('transactionDate').nullable()
  
              table.string('deemedExecutionDate').nullable()
              table.string('transactionFormType').nullable()
              table.string('transactionCode').nullable()
  
              table.string('equitySwapInvolved').nullable()
              table.string('transactionShares').nullable()
              table.string('transactionPricePerShare').nullable()
  
              table.string('transactionAcquiredDisposedCode').nullable()
              table.string('exerciseDate').nullable()
              table.string('expirationDate').nullable()
  
              table.string('underlyingSecurityTitle').nullable()
              table.string('underlyingSecurityShares').nullable()
              table.string('sharesOwnedFollowingTransaction').nullable()
              table.string('directOrIndirectOwnership').nullable()
            }).catch(e => logger.info(e))
        
     
     
    }
  }
