export type DBSECForm4 = {
  periodOfReport: string,
  id: string,
  tradingSymbol: string,
  notSubjectToSection16: string | null,
  reportingOwnerId: string,
  remarks: string |null,
  ownerSignatureName: string,
  ownerSignatureDate: string,
  isDirector:        string;
  isOfficer:         string;
  isTenPercentOwner: string;
  isOther:           string;
  officerTitle:      string;

}