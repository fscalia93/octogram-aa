export type DBNonDeriviateTable = {
  id: string,
  securityTitle:          string;
  transactionDate:        string;
  deemedExecutionDate:    string;
  transactionShares:               string;
  transactionPricePerShare:        string;
  transactionAcquiredDisposedCode: string;
  transactionFormType: string;
  transactionCode:     string;
  equitySwapInvolved:  string;
  sharesOwnedFollowingTransaction: string;
  directOrIndirectOwnership: string;
}