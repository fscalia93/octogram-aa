
export type DBReportingOwner = {
  id: string,
  name: string,
  rptOwnerStreet1:          string;
  rptOwnerStreet2:          string;
  rptOwnerCity:             string;
  rptOwnerState:            string;
  rptOwnerZipCode:          string;
  rptOwnerStateDescription: string;
}
