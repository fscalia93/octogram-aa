export type DBDEriviateTable = {
  id: string,
  securityTitle:             string;
  conversionOrExercisePrice: string;
  transactionDate:           string;
  deemedExecutionDate:       string;
  transactionFormType: string;
  transactionCode:     string;
  equitySwapInvolved:  string;
  transactionShares:               string;
  transactionPricePerShare:        string;
  transactionAcquiredDisposedCode: string;
  exerciseDate:              string;
  expirationDate:            string;
  underlyingSecurityTitle:  string;
  underlyingSecurityShares: string;
  sharesOwnedFollowingTransaction: string;
  directOrIndirectOwnership: string;
}