import { AdHoc } from "../ad-hoc/types/AdHoc"
import { DbAccess } from "./baseDbAccess"

const db = 'adhoc'

export class AdHocAccess extends DbAccess<AdHoc>{

  dbName = db


  updateElement = async(element: AdHoc): Promise<void> => {
    await this.knexInstance(db).where({title: element.title, pubDate: element.pubDate}).update(element)
  }

  create = async(): Promise<void> => {
    await this.knexInstance.schema.createTable('adhoc', (table) => {
      table.increments('id')
      table.string('title')
      table.string('pubDate')
      table.string('description')
      table.string('link')
      table.string('source')
    })
  }
  

}



