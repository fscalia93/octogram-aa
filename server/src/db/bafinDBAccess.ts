import { BafinEmittent } from "../insiderKäufer/type/BafinEmittent"
import { Emittent } from "../insiderKäufer/type/Emittent"
import { DbAccess } from "./baseDbAccess"
import knexInstance from "./knexInitialiser"

const db = 'insiderBafin'

export class InsiderDBAccess extends DbAccess<BafinEmittent>{


  dbName = db




  updateElement = async(element: BafinEmittent): Promise<void> => {
    await this.knexInstance(db).where({ISIN: element.issuerISIN, pubDate: element.id}).update(element)
  }

  create = async(): Promise<void> => {
    knexInstance.schema.createTable(db, (table) => {
      table.increments('rootId')
      table.string('issuerName')
      table.string('reportingOwner')
      table.string('publicationDate')
      table.string('issuerISIN')
      table.string('id')
      table.string('reportingOwnerStatus')
      table.string('transactionAcquiredDisposedCode')
      table.string('businessType')
      table.string('transactionPricePerShare')
      table.string('transactionShares')
      table.string('transactionDate')
      table.string('tradingPlace')
      table.string('activationDate')
      table.string('source')

    }).catch(e => console.log(e))
  }
  
}