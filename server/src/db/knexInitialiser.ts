import { Knex, knex } from 'knex'

const config: Knex.Config = {
  client: 'pg',
  connection: {
    host: 'db-postgresql-fra1-02342-do-user-3930384-0.b.db.ondigitalocean.com',
    user: 'doadmin',
    password:'qE7lF7KK8bs9ryho',
    database: 'defaultdb',
    port: 25060,
    ssl: {
      rejectUnauthorized: false,
    },
   
  },
};

 const knexInstance = knex(config);

 export default knexInstance


export const get = async<t>(table: string) => {
 const res = await knex<t>('table');
 return res;
}

export const create = async () => {

  knexInstance.schema.createTable('user', (table) => {
    table.increments('id')
    table.string('name')
    table.integer('age')
  })
  .then(() => console.log("Sucess")).catch((e) => console.log(e))

}

export const insert = async () => {
  await knexInstance('user')
  .insert({ name: 'hi@example.com' })
}

export const getTest = async () => {

  const res = await knexInstance<{}>('user');
  console.log(res)
  return res;
 }

