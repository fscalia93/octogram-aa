
"use strict"
var fcmWrapper = require("../DataAcessWraper/FcmWrapper")
var basicContr = require("./BasicController")

module.exports= {

    
    getFcmById(req, res){
        console.log("Get Feed by Id")
        var id = req.params.fcmId
        
        fcmWrapper.getFcmById(id).then(token => {
           
            basicContr.sendJson(token, res);

        })

    },
    
    addFcmByObject(req, res){
        var body = req.body;
        fcmWrapper.addFcmByObject(body);


    },
    
    addFcmById(req, res){
       var id = req.params.fcmId;
        fcmWrapper.addFcmById(id);

    },


    updateFcmByObject(req, res){
        console.log("Update FCM")
        var body = req.body;
        
        fcmWrapper.updateFcmByObject(body);


    }

}