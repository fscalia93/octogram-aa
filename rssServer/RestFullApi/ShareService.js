var express = require('express');

var router = express.Router();

module.exports = router
var ShareContr = require("./ShareController")


router.route("/")
.get(ShareContr.getShares)

router.route("/:shareName").get(ShareContr.getShareByName)
router.route("/s/:shareName").get(ShareContr.getSharesByName)
router.route("/id/:shareId").get(ShareContr.getShareById);