"use strict"
var feedWrapper = require("../DataAcessWraper/FeedWrapper")

        

module.exports = {
     getFeeds(req, res){
         feedWrapper.getFeeds().then(feed => {
          
            sendJson(feed, res)
        })

        
    },
    
    getFeed(req, res){
        var id = req.params.feedId;
        
        feedWrapper.getFeedById(id).then(feed => {
            
          
            sendJson(feed, res)
        })
    },
    getNextFeed(req, res){
        console.log("getNextFeed")
        var id = req.params.pubDate;
        feedWrapper.getNextFeed(id).then(feed => {
            
            sendJson(feed, res)
        })

    },
    getFeedsByShare(req, res){
        console.log("get FEEds by share")
       var id = req.params.shareId;
        feedWrapper.getFeedsByShare(id).then(feeds => {
            
            sendJson(feeds,res);
        })

    }

}

function sendJson(data, res){
    
    
    if(!data) res.send("Error Feed konnte nicht geladen werden")
        else {
            res.json(data);

        }
}

  
