var express = require('express');

var router = express.Router();

module.exports = router

var feedWController = require('./FeedController');

  
  router.get("/", feedWController.getFeeds);
    
  
  router.get('/:feedId', feedWController.getFeed)

  router.get('/next/:pubDate',  feedWController.getNextFeed)

  router.get("/share/:shareId", feedWController.getFeedsByShare)

