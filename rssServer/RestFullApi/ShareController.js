var shareWrapper = require("../DataAcessWraper/SahreWrapper")



module.exports = {

    getShareById(req, res){

        var id = req.params.shareId;
        
        shareWrapper.getShareById(id).then(share => {
            basicContr.sendJson(share, res)

        })
        
    },

    getShareByName(req,res){
       var  id = req.params.shareName
        shareWrapper.getShareByName(id).then(share => {
            basicContr.sendJson(share, res);

        })

    },
    getShares(req, res){
        shareWrapper.getShares().then(shares => {
            basicContr.sendJson(shares, res);

        })

    },
    getSharesByName(req, res){
        var id = req.params.shareName
        shareWrapper.getSharesByName(id).then(shares => {
            basicContr.sendJson(shares, res);
        })
    }
    
}