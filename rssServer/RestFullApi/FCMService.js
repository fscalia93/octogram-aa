var express = require('express');

var router = express.Router();

var fcmContr = require("./FCMController")

router.route("/")
.post(fcmContr.addFcmByObject)
.put(fcmContr.updateFcmByObject)

router.route("/:fcmId")
.get(fcmContr.getFcmById)
.post(fcmContr.addFcmById)

module.exports = router;
    
  
