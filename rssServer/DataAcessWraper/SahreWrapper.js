"use strict"
var dbAcess = require('./../DataAcess/SharesAcess')
var ObjectId = require('mongodb').ObjectID;
var symbolController = require("../api/controllers/symbolController")



module.exports= {

    addShare(share){
        shareWrapper(share);
        return dbAcess.addShare(share);


    },
    addShareByObject(share){
        return dbAcess.addShare(share)
    },
   async SyncaddShareByNameIfNotExist(name){
       var item = await this.getShareByName(name).then(async share => {
           
           
           if(share) return share;
           var addedItem  = await this.addShareByName(name)
         
           return addedItem.ops[0];

       })
       return item;


    },
    async SyncaddShareByShareIfNotExist(item){
        var returningShare = await this.getShareByName(item.name).then(async share => {

            console.log(share)
            
            
            if(share){
                return share;

            } 

            
            var othershares = await symbolController.getEndSymbols(item.name);

           

            
            var addingItem = item;
               
            
            addingItem["stockType"] = othershares;
            
            
           
                var addedItem  = await this.addShareByObject(addingItem);
                return addedItem.ops[0];
 

           
          
         
        })
        return returningShare;
 
 
     },
    addShareByName(name){
        
        return dbAcess.addShare(shareWrapperByName(name));

    },
    async addShareBySyncByName(name){
         var item = await  Promise.resolve(dbAcess.addShare(shareWrapperByName(name)));
         console.log(item)
        return item;

    },

    addShares(shares){
       
        //shares = shares.map(shareWrapper)
        return dbAcess.addShares(shares)

    },
    getShareById(id){
         var filter = {_id: ObjectId(id)}
        return dbAcess.getShare(filter);
    },
    getShareByName(name){
       
    var regex = this.getRegexName(name);
    var id = {"name": {$regex: regex , $options: "$i"}}
     
    return dbAcess.getShareByName(id);
    },
    getShareByRegex(name){

        var id = {"name": {$regex: name , $options: "$i"}}
     
    return dbAcess.getShareByName(id);

    },

    getShares(){
        return dbAcess.getShares();
    },
    async getSyncShares(){
        var items = await  Promise.resolve(dbAcess.getShares());
        return items;
    },
    getSharesByName(name){
       var regex = this.getRegexName(name);
        var id = {"name": {$regex: regex , $options: "$i"}};
        return dbAcess.getSharesByName(id)
        

    },
    getRegexName(name){
        return  new RegExp('.*' + RegExp.escape(name) + '.*')

    },

    getSharesRegex(shareName){
        return shareName.replace(/./, /[A-z]*/  )

    }


}
RegExp.escape = function(string) {
    return string.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&')
  };

function shareWrapper(share){

   



};

function shareWrapperByName(name){
    return {"name": name};
}


//Todooooo Regexxx

function dotregex(string){

    while(string.indexOf(/./) != -1){
        index = string.indexOf(/./)


    }

    string.replace(/./, /.*/)

    

}