var fcmdb = require("../DataAcess/FcmAcess")

"use strict";
module.exports = {

    getFCM(){
        return fcmdb.getFCMS()

    },
    addFcmById(id){
        return fcmdb.addFcmById(id);
    },
    addFcmByObject(item){

        
       return  this.getFcmById(item.tokenId).then(token => {
            if(!token){

                return fcmdb.addFcmByObject(item);
            }
            return token;
            
        })
       
    },
    getFcmById(id){
        
        var filter = {tokenId: id}
        return fcmdb.getFCMbyId(filter);

    },
    updateFcmByObject(fcm){
        
        
        var filter = {tokenId: fcm.tokenId}
        console.log(filter)
        return fcmdb.updateFCM(filter, fcm)
    }

}