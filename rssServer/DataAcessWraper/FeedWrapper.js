"use strict";

var dbAcess = require('../DataAcess/FeedAcess')
module.exports= {

    addFeed(feed){
         
        return dbAcess.addFeed(feed);


    },

    addFeeds(feeds){
        for (let index = 0; index < feeds.length; index++) {
            const element = feeds[index];
            feedWrapper(element);
            
        }
        return dbAcess.addFeeds(feeds)

    },
    async SyncaddFeedIfNotExist(feed){
        var item = await this.addFeedIfNotExist(feed);
        return item;
 
 
     },
     addFeedIfNotExist(feed){
         var feedObject = {"pubDate": feed.pubDate, "link": feed.link}
        return this.getFeedByObject(feedObject).then( getedFeed => {
            
            if(getedFeed){
                getedFeed["message"] =  "Feed existiert schon"
                getedFeed["status"] = 0;
                return  getedFeed;

            }
            else {
                return this.addFeed(feed).then(addedFeed => {
                    var item =  addedFeed.ops[0]
                    item["status"] = 1;
                    item["message"] = "Feed erfolgreich hinzugefügt"
                    
                    return item;
                })
              

            }
            
           
          
           
            
 
        })
        
 
 
     },
     updateFeed(feed){
         return dbAcess.updateFeed(feed);
     },

    
    getFeedById(id){        
        return dbAcess.getFeedById(id);


},
getFeedByObject(feed){
    return dbAcess.getFeedByObject(feed);
},
getFeeds(){

    return dbAcess.getFeeds();

},

getAllFeeds(){
    return dbAcess.getAllFeeds();
},

getNextFeed(id){
    return dbAcess.getNextFeed(id);

},
getFeedsByShare(name){
    console.log(name)
   var  filter = {"share.name": name}
    return dbAcess.getFeedsByShare(filter);
},
updateFeed(feed){

            


    return dbAcess.getFeedById(feed._id).then(oldFeed => {

        if(oldFeed){
            var feedToModify = modifyfeed(oldFeed, feed);
    
        return dbAcess.updateFeedById(feed._id, feedToModify)

        }
        else{
            return dbAcess.addFeed(feed);
        }

        

    }).catch(dbAcess.catchs)

 
}


}


function feedWrapper(feed){
    
}
function modifyfeed(oldfeed, updatingFeed){

    var newFeed = updatingFeed;
    var oldKeys = Object.keys(oldfeed)
    
    for (let index = 0; index < oldKeys.length; index++) {
        const element = oldKeys[index];

        if(!updatingFeed[element]){
            newFeed[element] = oldfeed[element]
        }
       
        
    }
    return newFeed;
    

}
