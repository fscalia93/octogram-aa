"use strict";
var dbAcess = require('./MongoDbAcess');
const FCM = "Token";



module.exports = {
    getFCMS(){ return  dbAcess.getAll(FCM) },

    getFCMbyId(id){

        return dbAcess.getOne(id, FCM);
    },

    addFCM(item){
       return  dbAcess.addOne(item, FCM);

    },
    updateFCM(filter, fcm){
        return dbAcess.findAnModifyOne(filter, fcm, FCM);
        

    }
    

}