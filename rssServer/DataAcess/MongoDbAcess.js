"use strict";
var MongoClient = require('mongodb').MongoClient;
var ObjectId = require('mongodb').ObjectID;





var url0 = "mongodb://localhost:27017/"
var url1 ="mongodb://server-deploy.code.hoctrade.de:27017/"
var isLocal = 0

var that = this
var url = isLocal? url1 : url0 


const DATABASE = "TradeHoc"
const FEED = 'Feed'
const SHARES = "Shares"


module.exports = {
  mongoClient: MongoClient,
  url: url,
  DATABASE: DATABASE,
  FEED: FEED,
  SHARES: SHARES,
  ObjectId: ObjectId,
  catchs(err){
    console.log("Catchss Funcktion")
    console.log(err)
},callback(err, result){
  if(err) console.log(err)

},
 thenArray : function thenArray(result){
  console.log(result)
  client.close();
  return result

},
 settings:  {
  reconnectTries : 10,
  autoReconnect : true
},

addMany(items,  reference){
  return MongoClient.connect(url, this.settings).then(function(client){
    
    var collection = client.db(DATABASE).collection(reference);

    

      let returnData = collection.insertMany(items, {w:1, ordered: 1}, that.callback);
      client.close();
      return returnData;
    
     
   
}).catch(this.catchs)
 },


 addOne(item,  reference){
  return MongoClient.connect(url, this.settings).then(function(client){
   

    var collection = client.db(DATABASE).collection(reference);

    let returnData
  

    returnData = collection.insertOne(item, {w:1},that.callback);
    

    client.close()
    return returnData;
    
  

  
}).catch(that.catchs)
 },

 getOne(filter, reference){
  return MongoClient.connect(url, this.settings).then(function(client){
    var collection = client.db(DATABASE).collection(reference);

    var item =  collection.findOne(filter)
    client.close()
    return item;
}).catch(that.catchs)

 },
 getOneById(id, reference){
   var filter = {_id : ObjectId(id)}
  return MongoClient.connect(url, this.settings).then(function(client){
    var collection = client.db(DATABASE).collection(reference);

    var item =  collection.findOne(filter)
    client.close()
    return item;
}).catch(that.catchs)

 },
 

 // To change
 getAll(reference){
  return MongoClient.connect(url, this.settings).then(function(client){
    var collection = client.db(DATABASE).collection(reference);


    
  
      
    return collection.find().toArray().then(result => {
      
    client.close();
  return result
    }).catch(that.catchs);
    
    
     
       
  
  }).catch(that.catchs);
  

 },
 getManyByFilter(filter, reference){
  return MongoClient.connect(url, this.settings).then(function(client){
    var collection = client.db(DATABASE).collection(reference);


    
    
    return collection.find(filter).toArray().then(that.thenArray).catch(that.catchs)
    
    
           
      }).catch(that.catchs);
  

 },
 findAnModifyOne(filter, item , reference){
  return MongoClient.connect(url, this.settings).then(function(client){

   var options =  {upsert: true}
    

    var collection = client.db(DATABASE).collection(reference);

    var update = collection.updateOne(filter,  {$set: item} , options)
    client.close();
    return update
     
}).catch(that.catchs)

 },
 countDoc(reference){
  return MongoClient.connect(url, settings).then(function(client){


    var collection = client.db(DATABASE).collection(reference);

   var count = collection.countDocuments()
   client.close();
   return count

 });
}
}



