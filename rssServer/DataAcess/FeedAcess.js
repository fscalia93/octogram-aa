"use strict";
var dbAcess = require('./MongoDbAcess')
var ObjectId = require('mongodb').ObjectID;

var FEED = "Feed"

var those = this;

module.exports = {
    addFeed(feed){ return dbAcess.addOne(feed, FEED); },
    
       
     addFeeds(feeds){ return dbAcess.addMany(feeds, FEED);},

    getFeedByObject(feed){
           return dbAcess.getOne(feed, FEED); },

        getFeedById(id){ return dbAcess.getOneById(id, dbAcess.FEED);},


        getAllFeeds(){  return dbAcess.getAll(dbAcess.FEED)
        },
        
        getFeeds(){
            return dbAcess.mongoClient.connect(dbAcess.url).then(function(client){
              var collection = client.db(dbAcess.DATABASE).collection(dbAcess.FEED);
 
                
                var feeds = collection.find().sort({pubDate:-1}).limit(20).toArray();
                client.close()
                 
                 return feeds;
            })


            .catch(dbAcess.catchs);
            

        },
        getNextFeed(pubDate){
            return dbAcess.mongoClient.connect(dbAcess.url).then(function(client){
                var collection = client.db(dbAcess.DATABASE).collection(dbAcess.FEED);
   
                  
                 



                       var  feeds = collection.find({"pubDate": {$lte:pubDate}}).sort({pubDate: -1}).limit(20).toArray();
                   
                   
                        return feeds;

                    

                  
              })
  
  
              .catch(dbAcess.catchs);

        },
        getFeedsByShare(filter){return dbAcess.getManyByFilter(filter, FEED); },
        updateFeedById(id, feed){

            var filter = {_id : ObjectId(id)}
            return dbAcess.findAnModifyOne(filter, feed, FEED);
        }
        
        

     
      
}

