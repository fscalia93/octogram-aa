"use strict";
var dbAcess = require('./MongoDbAcess');
const SHARES = "Shares";



module.exports = {
    addShare(share){

      
      return dbAcess.addOne(share, SHARES);
    },
    
       
       addShares(shares){
       
        return dbAcess.addMany(shares, SHARES);
       },
       getShares(){
         return dbAcess.getAll(SHARES)


       },
       getShare(id){
         return dbAcess.getOne(id, SHARES);

       },
       getShareByName(id){
         return dbAcess.getOne(id, SHARES);

       },
       getSharesByName(id){
        return dbAcess.getManyByFilter(id, SHARES);

       }

      
    

}