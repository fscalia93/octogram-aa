
require('use-strict')
"use strict";

var express = require('express');
var app = express();
var fcmdb = require('./fcmController')
var feedController = require("./api/controllers/feedController")
var share= require("./api/setShare")
var bodyParser = require("body-parser")
global.assert = require("assert")
global.basicContr = require("./RestFullApi/BasicController")
var feedService = require(".//RestFullApi/FeedService")
var shareService = require("./RestFullApi/ShareService")
var fcmService = require("./RestFullApi/FCMService")
var sharedb = require("./DataAcessWraper/SahreWrapper")
var feeddb = require("./DataAcessWraper/FeedWrapper")
//Must have for doing Intervall
var interVallHelper = require("./intervallHelper")


//Vor dem Rest 
var cors = require('cors')
app.use(cors())


app.use(bodyParser.json()); // for parsing application/json
app.use(bodyParser.urlencoded({ extended: true })); // for parsing application/x-www-form-urlencoded
//Vor dem Rest

app.use("/feed", feedService)
app.use("/share", shareService)
app.use("/Fcm", fcmService)



var server_port = process.env.OPENSHIFT_NODEJS_PORT || 8080
var server_ip_address = process.env.OPENSHIFT_NODEJS_IP || '0.0.0.0'
 


app.listen(server_port, server_ip_address, function () {
  console.log( "Listening on " + server_ip_address + ", port " + server_port )
});

global.oldNews = []
global.allSharesFromDb = []



sharedb.getShares().then(shares => {
  allSharesFromDb = shares
  intervalFunc();
  function intervalFunc() { 

    var url = 'https://www.finanznachrichten.de/rss-aktien-adhoc'
   
    feedController.getRSSFeed(url, finanzNachrichten)  
    
  
    
  }setInterval(intervalFunc, 10000);
  

  
});





function finanzNachrichten(err, result){
  var source = "Finanznachrichten"
  jsonParser(err, result,source)
}






async function jsonParser(err, result, source){

  if(err) return console.log(err)

   var json = result.rss.channel[0].item;

  
     
     
  for (let index = 0; index < json.length; index++) {



    var jsonObject = {
      "title": json[index].title[0],
      "pubDate":  json[index].pubDate[0],
      "description": json[index].description[0],
      "link": json[index].link[0],
      "source":source

         
    }

   
    if (oldNews.some(e => e.title == jsonObject.title && e.description == jsonObject.description)) {
    
      continue;
    }
    oldNews.push(jsonObject);

    
    
    await share.getShare(jsonObject).then(async item => {

     


      await feeddb.SyncaddFeedIfNotExist(item).catch(err => console.log(err)).then(feed => {

        if(feed.status){

          
          fcmdb.sendCloudMessaging(item);

        }
        console.log("Feedparser:", feed.message)
        
      
      });
    })

   

   

    
       
              
 
      
      

}


}











 




  
process.on('uncaughtException', function (err) {
  console.error(err.stack);
  console.log("Node NOT Exiting...");
});














