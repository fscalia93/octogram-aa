
const request = require('request-promise');
var parseString = require('xml2js').parseString;

module.exports = {

  getRSSFeed(url, callback){
    
    
      request(url, { json: false })
      .then(body => {

        parseString(body,callback) ;

      })
      .catch(e => { console.log(e) })

  }
}