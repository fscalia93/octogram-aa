"use strict"

var logoService = require("./logoService")
module.exports = {
   
   
    getLogo(name){
        var query = {"query": name,  json: true}
        return logoService.getLogoUrl(query)
    }



}