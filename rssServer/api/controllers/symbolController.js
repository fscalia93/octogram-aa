
var symbolService = require("./symbolService")
module.exports = {

    getSymbols(name){
        var query = {"keywords": name}
        return symbolService.getSymbols(query).then(res => {
           
            
            res = res.bestMatches.map(this.symbolWrapeper);
            
        

            if(res == []){
                console.log("Share not found alpha: ",name)
            }
           

            return res;
        })
        


    },
    async getEndSymbols(share){
        var item =   await this.getSyncSymbols(share).catch(async func => {
            
            if(func){
                if(func.code =="ENOTFOUND") {
                console.log(func)
            }

            if(func.Information ==  'Thank you for using Alpha Vantage! Please visit https://www.alphavantage.co/premium/ if you would like to have a higher API call volume.'){

            return this.getEndSymbols(share);
            }
        }
            else {
                return [];
            }

             

       });
       return item;
   },

  async getSyncSymbols(name){
      var items = await this.getSymbols(name).then(res => {
          return res
      })
      if(items){
          return items
      }else {

         return [];
      }
       

  },
  symbolWrapeper(element){
      var object = {
          "symbol": element["1. symbol"],
          "name":  element["2. name"],
          "type":  element["3. type"],
          "region":  element["4. region"],
          "marketOpen":  element["5. marketOpen"],
          "marketClose":  element["6. marketClose"],
          "timezone":  element["7. timezone"],
          "currency":  element["8. currency"],
          "matchScore":  element["9. matchScore"]  
        }
        return object;

  }
 

}
function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }
  
async  function letssleep(ms) {
    
     await sleep(ms);
    
  }
