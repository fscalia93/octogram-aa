"use strict";
var sharedb = require("../DataAcessWraper/SahreWrapper")
var symbolContr = require("./controllers/symbolController")
var shareAcess = require("../DataAcessWraper/SahreWrapper")
var categorieParser = require("../api/controllers/categorieParser")
var logoContr = require("../api/controllers/logoController")
var failed = []
var those= this;
//To Change
var that = this;
module.exports = {

    
    //Incoming from  RSSFEED
   async  getShare(news) {

    


    news["categorie"] =   categorieParser.parse(news)
       
        
        
    
        
        
        
           let  shareObject = this.getShareName(news)

           var logoUrl = await logoContr.getLogo(shareObject.logoName)

           
           if(logoUrl[0]) {

            shareObject.logoUrl = logoUrl[0].logo;
           }

          
           
            
            news.share = shareObject;
            
            news.searchText = this.getSearchText(shareObject.name);

    


            var filteredShares = allSharesFromDb.filter(x => x.name == shareObject.name);
            if(filteredShares.length> 0 ){
                
                

                
                shareObject.id = filteredShares[0]._id
                
               
               
            }
            else {
                
              
                var toAddingShare = shareObject
                

                let addedShare =  await sharedb.SyncaddShareByShareIfNotExist(toAddingShare)
              
                if(!addedShare) {
                   
                   console.log("Failll adding Shares")

                }
                else {

                    //console.log(addedShare)

               
                   
                
                    allSharesFromDb.push(addedShare)
                   
                    news.share.id = addedShare._id

        
                       
                         
                }
                   

                } 
                
               
            
             return Promise.resolve(news);

            
            

        },
        

getSearchText: function(share){
    var searchIndex = [0]
    var searchArray = []
    


    var pos = share.indexOf(' ');
    searchIndex.push(pos)

    while (pos != -1) {
        
        pos = share.indexOf(' ', pos + 1);
        searchIndex.push(pos)
}

    
    
   for (let i = 0; i < searchIndex.length  - 1 ; i++) {
       const index = searchIndex[i]; 
      


    for (let beginIndex = index+3; beginIndex < share.length; beginIndex++) {
       
            
        

            let text = share.substring(index, beginIndex+1)
            text = text.toLowerCase();
            
            if (text != "") searchArray.push(text)
        
    }
   }
       
  
    
    return searchArray
},

async getShareETRList(shares){

   var  acShares = shares;
    let newShares = []
    var i = 0;
    while(i < acShares.length){
        

        
        var  element = acShares[i];

        
        var shareName =  this.getShareName(element.instrument);
      
       var isin = element.ISIN;
       var wkn = element.WKN;
        var index = element.ProductAssignmentGroup;
        var indexDesc = element.ProductAssignmentGroupDescription;   
        var shareSymbols = element.Mnemonic;
        var regex = getRegex(shareName);
        var othershares = await symbolContr.getSyncSymbols(shareName).catch(err => console.log(err))
        

    

          

       var  shareObject = { "shareName": this.getShareName(element.instrument), "name": element.instrument,"searchtext": this.getSearchText(element.instrument), "regex": regex, "index": index, "indexDesc": indexDesc, "symbol": element.Mnemonic, 
        "ISIN": element.ISIN, "WKN": element.WKN,  stockType: othershares};

      
       var stockObject = this.getStocksByName(acShares , element.instrument);
        acShares= stockObject.restShares;

        shareObject.stockType =  stockObject.stocks;
        
        newShares.push(shareObject);

        shareAcess.addShare(shareObject).catch(err => {
            failed.push(i);
            console.log(i)
        })

        
      
        

        
    }
   function getRegex(name){
        return sharedb.getRegexName(name);
    }

           
    
   
    return newShares;
},
    

/*
Gibt alle Shares, die ähnlich zu dem übergegebenem Namen sind zurück
*/

    getStocksByName(shares, name){

        var filteredShares = shares.filter(x => ((x.instrument == name) || (this.getShareName(x.instrument) == this.getShareName(name))));
        //console.log("Fil", filteredShares.length)
        var mappedShares = filteredShares.map(element => {
            var x = {
            "name": element.instrument, 
            "symbol": element.Mnemonic, 
            "ISIN": element.ISIN, 
            "WKN": element.WKN,

        }})
        //console.log("Map", mappedShares.length)

        var otherShares = shares.filter(x => !(filteredShares.includes(x)));
       
        
            
           //console.log("Shares", shares.length, "Filter", filteredShares.length, "Map" ,mappedShares.length, "Other:", otherShares.length)

        
       

        return { "stocks": mappedShares ,  "restShares":otherShares }
        

    },



    

  
    
     getShareName( news){
        // Collection of all Companiesformats and other Stuff#

        var title = news.title
     
        
        var split = title.split(": ")

        if(!split[1]) {
            console.log(split)
        }



        
        var newName = split[1]+" ";
        



       
        
        
        //Option 1
       

        var share = this.parseCompany(newName)
        
            

        if(share){
            return share;
        }

            //Option2 
                var desc = news.description;

                split = desc.split(": ")
                

                for (let index = 0; index < split.length; index++) {
                    const element = split[index];
                    let returned = this.parseCompany(element)
                    if(returned) {
                        return returned;
                        
                    }
                    if(index == split.length){

                    
                        console.log("Not found:", split)
                    
                    }
                    
                    
                }

                return newName;         
                  
  
   
    
    


    },

    parseCompany(newName){

       var companyObject = {
            name: undefined,
            logoUrl: undefined,
            logoName: undefined
        }

        newName = newName + " "

        newName = newName.replace("Aktiengesellschaft", " AG ");

        
        if(newName.indexOf("(deutsch)") != -1){
            newName = newName.substring(0, newName.indexOf("(deutsch)"));

        }



        var parseName = newName.toLowerCase()

       

        

        
                
            for (let index = 0; index < companyNamearray.length; index++) {
                const element = companyNamearray[index].toLowerCase();
                
            
            if(!(parseName.indexOf(element) == -1)){
        
                 
                companyObject.logoName =   newName.substring(0, parseName.indexOf(element)).trim();
                
                companyObject.name = newName.substring(0, parseName.indexOf(element)+ element.length).trim();

                return companyObject;
                
                
                
    
            }
        }
       
        return null;
    }

};



var   companyNamearray  = [" AG ", " SE ",  " N.V. " , "-AG ", " Group ", " Inc ", "Incorporated"," Plc. ", " plc " , " Aktiengesellschaft ", " gGmbH " ]

    String.prototype.replaceAt=function(index, replacement) {
        return this.substr(0, index) + replacement+ this.substr(index + replacement.length);
    }

    
      
     
    